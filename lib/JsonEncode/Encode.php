<?php
/**
 * Created by PhpStorm.
 * User: repla
 * Date: 17.05.2018
 * Time: 23:48
 */

namespace api\JsonEncode;
/**
 * Created by PhpStorm.
 * User: repla
 * Date: 08.05.2018
 * Time: 0:06
 */

class Encode
{
    public $getJson;
    public $default;

    /**
     * @return string
     */
    public function toJson()
    {
        return json_encode($this->default);
    }
}