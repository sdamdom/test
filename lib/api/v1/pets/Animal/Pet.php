<?php
namespace api\v1\pets\Animal;
use api\Reader\Read;
/**
 * Created by PhpStorm.
 * User: repla
 * Date: 18.08.2018
 * Time: 15:53
 */
class Pet {
    public $name;
    public $age;
    public $type;
    public $inputData;


    /**
     * Pet constructor.
     * @param Read $inputApi
     * @throws \Exception
     */
    public function __construct(Read $inputApi)
    {
        if (!$inputApi->authData['name']) {
            throw new \RuntimeException('Enter name', ACCESS_DENIED);
        }
        if (!$inputApi->authData['age']) {
            throw new \RuntimeException('Enter age', ACCESS_DENIED);
        }
        if (!$inputApi->authData['type']) {
            throw new \RuntimeException('Select type', ACCESS_DENIED);
        }
        $this->name = $inputApi->authData['name'];
        $this->age = $inputApi->authData['age'];
        $this->type = $inputApi->authData['type'];
        $this->inputData = $inputApi->authData;
    }
}