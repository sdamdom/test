<?php
namespace api\v1\pets\Insert;
use api\JsonEncode\Encode;
use api\Reader\Read;
use api\v1\pets\Animal\Pet;
use \RedBeanPHP\R as R;
/**
 * Created by PhpStorm.
 * User: repla
 * Date: 18.08.2018
 * Time: 14:07
 */

class InsertPet extends Pet
{
    public $id;

    public function createObject(Encode $encode)
    {
        $this->create();
        $this->ok($encode);

    }

    private function create()
    {
        //RedBeanPhp
        $user = R::load(USER_TABLE,$this->inputData['userId']);
        $pet = R::dispense(PET_TABLE);
        $pet->user = $user;
        $pet->name = $this->name;
        $pet->age = $this->age;
        $pet->type = $this->type;
        $pet->fromDate = date(DATE_RFC822);
        $pet->status = true;
        $this->id = R::store($pet);
    }
    private function ok(Encode $encode)
    {
        $encode->default = ['status' => true, 'lastCreate' => $this->id];
    }
}