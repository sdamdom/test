<?php
namespace api\v1\pets\Read;
use api\JsonEncode\Encode;
use api\Reader\Read;
/**
 * Created by PhpStorm.
 * User: repla
 * Date: 18.08.2018
 * Time: 14:23
 */
class PetList
{
    private $inputApi;
    private $oldPetId;

    public function __construct(Read $inputApi)
    {
        $this->inputApi = $inputApi->authData;
    }

    public function movePet(Encode $encode)
    {
        $this->getOneByOld();
        $this->move($encode);
    }
    private function getOneByOld()
    {
        $pet = R::findLike(PET_TABLE, [
            'type' => $this->inputApi['type'],
            'status' => true
        ], ' ORDER BY date DESC LIMIT 1 ' );
        $this->oldPetId = $pet['id'];
    }
    private function move($encode)
    {
        $user = R::load(USER_TABLE,$this->inputApi['userId']);
        $pet = R::load(PET_TABLE,$this->oldPetId);
        $pet->status = false;
        $pet->master = $user;
        R::store($pet);
        $encode->default = ['status' => true, 'massage' => $pet['name'].'=>'.$user['name']];
    }

    /**
     * @param Encode $encode
     * @return mixed
     */
    public function getList(Encode $encode)
    {
        //Делаем выборку из БД по входным данным
        $list = R::findLike(PET_TABLE, [
            'type' => $this->inputApi['type'],
            'status' => true
        ], ' ORDER BY name ' );
        $encode->default = $list;
        return true;
    }
}